<?php 
include '../../config.php';
include '../../db/db.php';

if(isset($_GET['s_id']) ){
	$student_id = trim($_GET['s_id']);
	
	$query = "SELECT aup_c.course_id, aup_c.course_name, aup_c.course_number from aup_courses aup_c, aup_student_academics aup_sa, aup_undergrad_major_requirement aup_umr
			  where aup_umr.major_id=aup_sa.st_major 
			  and aup_c.course_id = aup_umr.major_req_course_id
			  and aup_sa.st_id = :s_id";

   $result = $GLOBALS['db']->query($query)->bind(':s_id',$student_id)->select();
   foreach ($result as $key) 
   	echo '<option value='. $key['course_id'] .'>'. trim($key['course_name']).'</option>';
}

?>