<?php
/*  session_start();

  if(isset($_SESSION['username'])){
    header("location:../index.php");
  }*/
include '../../config.php';
include '../../db/db.php';


$db = new db($username,$password,$db_name);
$dbdebug  = $dbdebug = new DBdebug();

if(isset($_POST)){
  if($_POST['requirement_type']=='') return;
  $query = "INSERT INTO aup_undergrad_major_requirement (";
    // var_dump($_POST);die();
  $count = 0;
  foreach ($_POST as $courseInfo => $value) {
    $count ++;
    $query .= $courseInfo ;
    if($count != count($_POST))
      $query .=  ',';
  }
  $query .= ") VALUES (";
  $count = 0;
  $courseName = '';
  foreach ($_POST as $courseInfo => $value) {
    $count ++;
    if($count==4)
      $courseName = $value;
    $query .= "'".trim($value) ."'";
    if($count != count($_POST))
      $query .=  ',';
  }
  $query .= ") ";
  // die($query)  ;
  $prepare = $db->query($query);
  $result = $prepare->execute();
  


  if($result){
    echo "New requirement added!<br>"; 
    
  }

  
}


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Add Major/Minor Requirement</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="../../css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="../../css/main.css" rel="stylesheet" media="screen">
  </head>

<body>
<div class="container">

     
<form class="form-horizontal" method="POST">
<fieldset>

<!-- Form Name -->
<legend>Add Major/Minor Requirement</legend>

<!-- Select Basic -->
<?php 
$requestCourses = $db->query('select mid,major_name from aup_undergrad_majors ')->select();
$html = "";

foreach ($requestCourses as $course ) {
  $html .= '<option value='. $course['mid'] .'>'. $course['major_name'].'</option>';
}
?>
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Undergrad Programs</label>
  <div class="col-md-4">
    <select name="major_id"  class="form-control">
      <?php echo $html ?>
    </select>
  </div>
</div>





<?php 
$requestCourseLevels = $db->query('select course_id, course_number,course_name from  aup_courses ')->select();
$html = "";
foreach ($requestCourseLevels as $courseLevel ) {
  $html .= '<option value='. $courseLevel['course_id'] .'>'. $courseLevel['course_number']." <> " .  $courseLevel['course_name'].'</option>';
}
?>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Undergrad Courses</label>
  <div class="col-md-4">
    <select name="major_req_course_id"  class="form-control">
      <?php echo $html ?>
    </select>
  </div>
</div>

<?php 
$requestCourseLevels = $db->query('SELECT * FROM aup_undergrad_course_type')->select();
$html = '<option selected value="null">None</option>';
foreach ($requestCourseLevels as $courseLevel ) {
  $html .= '<option value='. $courseLevel['id'] .'>'. $courseLevel['requirement_name'].'</option>';
}
?>
<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Course type</label>
  <div class="col-md-4">
    <select name="requirement_type"  class="form-control">
      <?php echo $html ?>
    </select>
  </div>
</div>


  <div class="form-group">        
      <div class="col-md-6 control-label">
        <button type="submit" class="btn btn-default">Submit</button>
      </div>
    </div>



</fieldset>
</form>


    </div> <!-- /container -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="../../login/js/bootstrap.js"></script>
    <!-- The AJAX login script -->
    <script src="../../login/js/login.js"></script>

  </body>
</html>


