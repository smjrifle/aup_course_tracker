<?php
 session_start();
//PUT THIS HEADER ON TOP OF EACH UNIQUE PAGE
  
  if(!isset($_SESSION['username'])){
    header("location:login/main_login.php");
  }

  include 'config.php';
  include 'db/db.php';
  include 'scripts/functions.php';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="css/main.css" rel="stylesheet" media="screen">
  </head>
  <body>
    <div class="container">
  
      <!-- Index for the course -->
      <table id="status">
      <?php 
        $courseToColorLookup = getCourseStatusList(); 
        foreach ($courseToColorLookup as $statusColor):
      ?>
      <tr>
        <td  style="background-color:<?php echo $statusColor['status_color'] ?>"></td>
        <td><?php echo $statusColor['status_name'] ?></td>
      </tr>
      <?php endforeach ?>
      </table>
      <div class="row">
          <div class="col-sm-4">
          <div class="row">
            <h3>Welcome  
             <strong> <?php echo $_SESSION['username'] ?> </strong>! </h3>
          </div>  
            <div class="row">
              <strong>Major declared </strong> : 
                    <?php $majors = getStudentMajor($_SESSION['username']); 
                      foreach ($majors as $major):?>
                        <h3> <?php echo $major['major_name'] ?></h3> 
                      <?php endforeach  ?>
              <strong>Minor declared </strong> : 
                    <?php $minors = getStudentMinor($_SESSION['username']); 
                      foreach ($minors as $minor):?>
                        <h3> <?php echo $minor['minor_name'] ?></h3> 
                      <?php endforeach  ?>
              <strong>Class standing </strong> : 
                  <?php $standing = getStudentStanding($_SESSION['username']); 
                      foreach ($standing as $standing):?>
                        <h3> <?php echo $standing['name'] ?></h3> 
                      <?php endforeach  ?>
            </div>
          </div>
          
          
          <div class="col-sm-8">
            <div class="row">
              <div class="col-sm-2 logout">
                <a href="login/logout.php" class="btn btn-default ">Logout</a> 
              </div>
            </div>
            <h3>Courses</h3>
            <?php 
            $majorids = getStudentMajor($_SESSION['username']);
            foreach ($majorids as $major ) {
              $majorid = $major['mid'];
            } 

            $minorids = getStudentMajor($_SESSION['username']);
            foreach ($minorids as $minor ) {
              $minorid = $minor['mid'];
            } 

            ?>
            <div class="col-sm-6">
              <div class="row">
                <h4><b>General Education</b></h4>
                 <ol type='1'>
                 <?php
                      $credits = 0; 
                      $requirements  = getRequirements($majorid,'1');
                      foreach ($requirements as $requirement): ?>
                  <li>
                    <font color=
                    "<?php $temp = getStatusIDtoColor($requirement['course_status']);foreach ($temp as $to ){$color = $to['status_color'];} echo $color;        
                  ?>">
                    <?php echo strtoupper( $requirement['course_name'] ).
                      '[' . $requirement['course_number']. ']' . 
                      ' <b>' . $requirement['course_credit'] . ' </b> Credit(s)'  ;
                     $credits += intval($requirement['course_credit']);
                    ?> 
                  </font>
                  </li>
                 <?php endforeach ?>
                </ol> 
                <b>Credits Total : </b> <?php echo $credits ?>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="row">
                  <h4><b>Core requirements</b></h4>
                 <ol type='1'>
                <?php
                 $creditsCore = 0; 
                 $requirements  = getRequirements($majorid,'2');
                      foreach ($requirements as $requirement):
                  ?>
                   <li>
                   <font color=
                    "<?php $temp = getStatusIDtoColor($requirement['course_status']);foreach ($temp as $to ){$color = $to['status_color'];} echo $color;        
                  ?>">
               
                  <?php
                   echo strtoupper( $requirement['course_name'] ).
                    '[' . $requirement['course_number']. ']' . 
                    ' <b>' . $requirement['course_credit'] . ' </b> Credit(s)'  ;
                   $creditsCore += intval($requirement['course_credit']);
                  ?> 
                  </font>
                  </li>
                 <?php endforeach ?>
                </ol> 
                 <b>Credits Total : </b> <?php echo $creditsCore ?>
              </div>
            </div>
              <b>Grand Total :  </b> <?php echo $credits + $creditsCore ?> Credits        
          </div>
      </div>
    </div> <!-- /container -->
  </body>
</html>
