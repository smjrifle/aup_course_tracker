<?php
session_start();
//PUT THIS HEADER ON TOP OF EACH UNIQUE PAGE

if(!isset($_SESSION['username'])){
  header("location:login/main_login.php");
}

include 'config.php';
include 'db/db.php';
include 'scripts/functions.php';
$maxCredit = 5;
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Dashboard</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Bootstrap -->
  <link href="css/bootstrap.css" rel="stylesheet" media="screen">
  <link href="css/main.css" rel="stylesheet" media="screen">
  <link href='https://fonts.googleapis.com/css?family=Raleway:400,200' rel='stylesheet' type='text/css'>

</head>
<body>
  <header>
    <nav class="navbar navbar-default navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="/studentprofile">
            <a href="/"><img src="image/logo.png" alt="" /></a>
          </a>
          <div class="col-sm-4 logout pull-right">
            <span><?php echo 'Welcome, ' . $_SESSION['username']; ?></span>
            <a href="schedule.php?st_id=<?php echo $_SESSION['username']?>" class="">Schedule</a>
            <span></span>
            <a href="login/logout.php" class="">Logout</a>
          </div>
        </div>
      </div>
    </nav>
  </header>
  <div class="container">
    <div class="courses clearfix">
      <h4>Student Detail</h4>
      <div class="courses-wrapper clearfix">
        <div class="col-sm-4">
         <div class="col-sm-12">
          <h5>Major declared</h5>
          <?php $majors = getStudentMajor($_SESSION['username']);
          foreach ($majors as $major):?>
          <p> <?php echo $major['major_name'] ?></p>
        <?php endforeach  ?>
      </div>
    </div>
    <div class="col-sm-4">
     <div class="col-sm-12">
      <h5>Minor declared</h5>
      <?php $minors = getStudentMinor($_SESSION['username']);
      foreach ($minors as $minor):?>
      <p> <?php echo $minor['minor_name'] ?></p>
    <?php endforeach  ?>
  </div>
</div>
<div class="col-sm-4">
 <div class="col-sm-12">
  <h5>Class Standing</h5>
  <?php $standing = getStudentStanding($_SESSION['username']);
  foreach ($standing as $standing):?>
  <p> <?php echo $standing['name'] ?></p>
<?php endforeach  ?>
</div>
</div>
</div>
</div>


<div class="course-detail">
  <?php
  $majorids = getStudentMajor($_SESSION['username']);
  foreach ($majorids as $major ) {
    $majorid = $major['mid'];
  }

  $minorids = getStudentMajor($_SESSION['username']);
  foreach ($minorids as $minor ) {
    $minorid = $minor['mid'];
  }
  ?>
  <hr/>
  <!-- Index for the course -->
  <div class="row no-margin section">
    <h4>Courses</h4>
    <div class="row no-margin">
      <h5>General Education</h5>
      <div class="subject clearfix">
        <?php
        $credits = 0;
        $requirements  = getRequirements($majorid,'1');
        foreach ($requirements as $requirement): ?>
        <div class="col-sm-4 subject-inner">
          <div class="col-sm-12 no-padding">
            <div class="flex-wrap">
              <div class="flex-content">
                <h4><?php echo trim(strtoupper( $requirement['course_name'] ));
                  $credits += intval($requirement['course_credit']);
                  $colSize = ($requirement['course_credit'] / $maxCredit) * 100;
                  ?> </h4>
                </div>
              </div>
              <div style="width: <?php echo $colSize;?>%; float: none;height: 10px;background-color:
                <?php $temp = getStatusIDtoColor($requirement['course_status']);foreach ($temp as $to ){$color = $to['status_color'];} echo $color;
                ?>"></div>
                <div class="statbox clearfix">
                  <div class="stats-contributors col-xs-6"><p><?php echo $requirement['course_number'];?></p><p><small>Course Number</small></p></div>
                  <div class="stats-contributors col-xs-6"><p><?php echo $requirement['course_credit'];?></p><p><small>Credit(s)</small></p></div>
                </div>
              </div>
            </div>
          <?php endforeach ?>
        </div>
        <p class="total pull-right"><b>Credits Total : </b> <?php echo $credits ?></p>
      </div>
    </div>
    <div class="row no-margin section">
      <h5>Core Requirements</h5>
      <div class="subject clearfix">
        <?php
        $creditsCore = 0;
        $requirements  = getRequirements($majorid,'2');
        foreach ($requirements as $requirement): ?>
        <div class="col-sm-4 subject-inner">
          <div class="col-sm-12 no-padding">
            <div class="flex-wrap">
              <div class="flex-content">
                <h4><?php echo trim(strtoupper( $requirement['course_name'] ));
                  $creditsCore += intval($requirement['course_credit']);
                  $colSize = ($requirement['course_credit'] / $maxCredit) * 100;
                  ?> </h4>
                </div>
              </div>
              <div style="width: <?php echo $colSize;?>%;float: none;height: 10px;background-color:
                <?php $temp = getStatusIDtoColor($requirement['course_status']);foreach ($temp as $to ){$color = $to['status_color'];} echo $color;
                ?>"></div>
                <div class="statbox clearfix">
                  <div class="stats-contributors col-xs-6"><p><?php echo $requirement['course_number'];?></p><p><small>Course Number</small></p></div>
                  <div class="stats-contributors col-xs-6"><p><?php echo $requirement['course_credit'];?></p><p><small>Credit(s)</small></p></div>
                </div>
              </div>
            </div>
          <?php endforeach ?>
        </div>
        <p class="total pull-right"><b>Credits Total : </b> <?php echo $creditsCore ?></p>
      </div>
    </div>

    <div class="row"> <h4 class="pull-right"><b>Grand Total : </b> <?php echo intval($creditsCore + $credits); ?></h4></div>
    <div class="index clearfix">
      <h6 style="">Color Code Index</h6>
      <?php
//Current #000  Finished #2B7F39  Not Taken #D15656
      $courseToColorLookup = getCourseStatusList();
      $classSize = (int) 12/count($courseToColorLookup);
      $class = 'col-sm-' . $classSize;
      foreach ($courseToColorLookup as $statusColor):
        ?>
      <div class="col-xs-2 no-padding"><span class="color-bar <?php echo $class;?> indexes" style="background-color:<?php echo $statusColor['status_color'] ?>"></span><p><?php echo $statusColor['status_name'] ?></p></div>
      <?php
      endforeach;
      ?>
    </div>
  </div> <!-- /container -->
</body>
<!-- Google Analytics -->
   <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-64055367-2', 'auto');
    ga('send', 'pageview');
  </script>
  <!-- End Google Analytics -->
</html>
