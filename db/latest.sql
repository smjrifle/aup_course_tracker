-- MySQL dump 10.13  Distrib 5.5.47, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: aup_course_tracker
-- ------------------------------------------------------
-- Server version	5.5.47-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aup_courses`
--

DROP TABLE IF EXISTS `aup_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aup_courses` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_number` varchar(200) NOT NULL,
  `course_name` varchar(200) NOT NULL,
  `course_credit` int(11) NOT NULL,
  `course_pre_req` varchar(200) NOT NULL,
  `course_type` int(11) NOT NULL,
  PRIMARY KEY (`course_id`),
  KEY `course_type_2` (`course_type`),
  CONSTRAINT `CourseRepeatType` FOREIGN KEY (`course_type`) REFERENCES `aup_courses_repeat_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aup_courses`
--

LOCK TABLES `aup_courses` WRITE;
/*!40000 ALTER TABLE `aup_courses` DISABLE KEYS */;
INSERT INTO `aup_courses` VALUES (2,'MA1020',' APPLIED STATISTICS I ',4,'null',1),(3,'CS1040','INTRO TO COMPUTER PROGRAMMING I',5,'null',1),(4,'CS1050',' INTRO TO COMPUTER PROGRAMMING II',5,'null',1),(5,'CS2071',' LANGUAGES & DATA STRUCTURES',4,'null',1),(6,'CS3048','HUMAN-COMPUTER INTERACTION ',4,'null',1),(7,'CS3068','DATABASE APPLICATIONS ',4,'null',1),(8,'CS3051','WEB APPLICATIONS',4,'null',1),(9,'MA2400','DISCRETE MATHEMATICS',4,'null',1),(10,'CS3032','OPERATING SYSTEMS',4,'null',1),(11,'CS3051','WEB APPLICATIONS',4,'null',1),(12,'CS3053','SOFTWARE ENGINEERING',4,'null',1),(13,'CS4095','SENIOR PROJECT',1,'null',1),(14,'EN1010','COLLEGE WRITING',4,'null',1),(15,'EN2020','WRITING & CRITICISM',4,'null',1),(16,'FR1200','ELEMENTARY FRENCH AND CULTURE II',4,'null',1),(17,'FR1100','ELEMENTARY FRENCH AND CULTURE I',4,'null',1),(18,'CS3026','ARTIFICIAL INTELLIGENCE',4,'null',1),(19,'AR1010','INTRO TO DRAWING',4,'null',1),(20,'CS3015','Computer Networks',4,'null',1),(33,'LI1000','LANGUAGE ACQUISITION AND SOCIAL POLICY',4,'null',1),(34,'PO1091','THE UNITED STATES AND THE WORLD',4,'null',1),(35,'INPR4095','SENIOR PROJECT',1,'null',1),(38,'MA 1020','Statistics',4,'null',1),(39,'EN2020','WRITING & CRITICISM',4,'null',1);
/*!40000 ALTER TABLE `aup_courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aup_courses_repeat_type`
--

DROP TABLE IF EXISTS `aup_courses_repeat_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aup_courses_repeat_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repeat_type` varchar(200) NOT NULL,
  `repeat_desc` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aup_courses_repeat_type`
--

LOCK TABLES `aup_courses_repeat_type` WRITE;
/*!40000 ALTER TABLE `aup_courses_repeat_type` DISABLE KEYS */;
INSERT INTO `aup_courses_repeat_type` VALUES (1,'REGULAR','Lorem Ipsum'),(2,'EVERY SPRING','');
/*!40000 ALTER TABLE `aup_courses_repeat_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aup_programs`
--

DROP TABLE IF EXISTS `aup_programs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aup_programs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `program_name` varchar(200) NOT NULL,
  `program_desc` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aup_programs`
--

LOCK TABLES `aup_programs` WRITE;
/*!40000 ALTER TABLE `aup_programs` DISABLE KEYS */;
INSERT INTO `aup_programs` VALUES (1,'Undergraduate','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet mi luctus lorem hendrerit pharetra. In hac habitasse platea dictumst. Aenean et porta turpis, ac tempor ex. Morbi convallis condimentum sapien ut pellentesque. Aenean luctus, magna eget volutpat aliquet, nulla augue mattis nunc, non molestie quam sapien ac justo. Proin eu turpis non massa cursus dictum. Ut ullamcorper, enim id interdum condimentum, eros tortor sagittis nisi, ultricies vestibulum metus turpis vel dolor. Phasellus pharetra volutpat leo, id blandit tellus luctus sed. Suspendisse et ex tristique, porttitor mi at, hendrerit leo. In tincidunt nunc a dui aliquam porttitor. Maecenas consectetur sem tellus, vitae dignissim orci molestie sit amet. In dapibus vehicula velit, eu ultrices orci egestas ut. Integer sed nibh sit amet elit dictum varius vel at elit. Nunc vel nisl metus. Mauris sit amet scelerisque augue, eu iaculis diam.\r\n\r\nVestibulum vitae efficitur nibh. In mauris metus, mattis convallis quam nec, efficitur porttitor dui. Mauris nunc nulla, mollis at aliquam nec, eleifend et dolor. Vestibulum velit tellus, volutpat non vehicula at, pulvinar nec sem. Etiam vitae tristique ipsum, sed mollis velit. Aenean vel sem eu lectus ornare cursus. Mauris diam nisi, vehicula quis nulla eu, mollis volutpat purus.\r\n\r\nNullam sed volutpat quam, sed vestibulum elit. Etiam eu lorem eget augue scelerisque rhoncus. Aliquam consequat libero elit, quis tincidunt arcu interdum a. Nam pretium egestas diam dignissim commodo. Sed vel mauris placerat ipsum tempor imperdiet. Nunc at tempus metus. Nam ultrices leo malesuada, viverra turpis eget, convallis justo. Sed suscipit et ante sit amet lobortis. Vivamus mattis odio id sapien tincidunt ultricies. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut eu vehicula mi. Quisque tristique massa velit, eu venenatis sem mattis dignissim.\r\n\r\nQuisque euismod magna a dolor lacinia, eget dictum sem dictum. Mauris elementum ipsum eu mi facilisis, ac ultricies nisl molestie. Cras et lacinia erat. Donec pellentesque neque velit, sit amet pretium neque gravida vitae. Nulla facilisi. In rutrum vulputate odio id euismod. Quisque a magna sit amet nunc venenatis cursus. Curabitur mattis gravida mauris quis porta.\r\n\r\nIn non posuere justo, nec porta ipsum. Sed convallis efficitur ligula. Proin ut ullamcorper nulla. Mauris blandit condimentum nisi vel blandit. Proin quis metus non nisl blandit vehicula. Maecenas ullamcorper sit amet ligula vel pulvinar. Aliquam erat volutpat. Aliquam eget enim eu sapien congue convallis sed vel dolor. Aliquam aliquet massa eget mi volutpat, eget lobortis libero euismod. Etiam mollis, lectus non porta tincidunt, purus arcu viverra sapien, at posuere orci magna ac diam. Praesent fringilla, metus eget pulvinar sodales, dui orci condimentum erat, ut accumsan lectus nibh quis mauris. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean non luctus metus. Proin non pretium ligula. In fermentum risus in ante malesuada faucibus. Donec nec metus rhoncus, ultricies urna rutrum, aliquet nunc. '),(2,'Graduate','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet mi luctus lorem hendrerit pharetra. In hac habitasse platea dictumst. Aenean et porta turpis, ac tempor ex. Morbi convallis condimentum sapien ut pellentesque. Aenean luctus, magna eget volutpat aliquet, nulla augue mattis nunc, non molestie quam sapien ac justo. Proin eu turpis non massa cursus dictum. Ut ullamcorper, enim id interdum condimentum, eros tortor sagittis nisi, ultricies vestibulum metus turpis vel dolor. Phasellus pharetra volutpat leo, id blandit tellus luctus sed. Suspendisse et ex tristique, porttitor mi at, hendrerit leo. In tincidunt nunc a dui aliquam porttitor. Maecenas consectetur sem tellus, vitae dignissim orci molestie sit amet. In dapibus vehicula velit, eu ultrices orci egestas ut. Integer sed nibh sit amet elit dictum varius vel at elit. Nunc vel nisl metus. Mauris sit amet scelerisque augue, eu iaculis diam.\r\n\r\nVestibulum vitae efficitur nibh. In mauris metus, mattis convallis quam nec, efficitur porttitor dui. Mauris nunc nulla, mollis at aliquam nec, eleifend et dolor. Vestibulum velit tellus, volutpat non vehicula at, pulvinar nec sem. Etiam vitae tristique ipsum, sed mollis velit. Aenean vel sem eu lectus ornare cursus. Mauris diam nisi, vehicula quis nulla eu, mollis volutpat purus.\r\n\r\nNullam sed volutpat quam, sed vestibulum elit. Etiam eu lorem eget augue scelerisque rhoncus. Aliquam consequat libero elit, quis tincidunt arcu interdum a. Nam pretium egestas diam dignissim commodo. Sed vel mauris placerat ipsum tempor imperdiet. Nunc at tempus metus. Nam ultrices leo malesuada, viverra turpis eget, convallis justo. Sed suscipit et ante sit amet lobortis. Vivamus mattis odio id sapien tincidunt ultricies. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut eu vehicula mi. Quisque tristique massa velit, eu venenatis sem mattis dignissim.\r\n\r\nQuisque euismod magna a dolor lacinia, eget dictum sem dictum. Mauris elementum ipsum eu mi facilisis, ac ultricies nisl molestie. Cras et lacinia erat. Donec pellentesque neque velit, sit amet pretium neque gravida vitae. Nulla facilisi. In rutrum vulputate odio id euismod. Quisque a magna sit amet nunc venenatis cursus. Curabitur mattis gravida mauris quis porta.\r\n\r\nIn non posuere justo, nec porta ipsum. Sed convallis efficitur ligula. Proin ut ullamcorper nulla. Mauris blandit condimentum nisi vel blandit. Proin quis metus non nisl blandit vehicula. Maecenas ullamcorper sit amet ligula vel pulvinar. Aliquam erat volutpat. Aliquam eget enim eu sapien congue convallis sed vel dolor. Aliquam aliquet massa eget mi volutpat, eget lobortis libero euismod. Etiam mollis, lectus non porta tincidunt, purus arcu viverra sapien, at posuere orci magna ac diam. Praesent fringilla, metus eget pulvinar sodales, dui orci condimentum erat, ut accumsan lectus nibh quis mauris. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean non luctus metus. Proin non pretium ligula. In fermentum risus in ante malesuada faucibus. Donec nec metus rhoncus, ultricies urna rutrum, aliquet nunc. ');
/*!40000 ALTER TABLE `aup_programs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aup_student_academics`
--

DROP TABLE IF EXISTS `aup_student_academics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aup_student_academics` (
  `st_id` varchar(11) NOT NULL,
  `st_major` int(11) DEFAULT NULL,
  `st_minor` int(11) DEFAULT NULL,
  `st_standing` int(11) NOT NULL,
  PRIMARY KEY (`st_id`),
  KEY `st_major` (`st_major`),
  KEY `st_standing` (`st_standing`),
  KEY `st_minor` (`st_minor`),
  CONSTRAINT `studentToMajor` FOREIGN KEY (`st_major`) REFERENCES `aup_undergrad_majors` (`mid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `studentToMinor` FOREIGN KEY (`st_minor`) REFERENCES `aup_undergrad_minor` (`mid`),
  CONSTRAINT `studentToStanding` FOREIGN KEY (`st_standing`) REFERENCES `aup_student_standing` (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aup_student_academics`
--

LOCK TABLES `aup_student_academics` WRITE;
/*!40000 ALTER TABLE `aup_student_academics` DISABLE KEYS */;
INSERT INTO `aup_student_academics` VALUES ('a94542',1,1,2);
/*!40000 ALTER TABLE `aup_student_academics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aup_student_academics_history`
--

DROP TABLE IF EXISTS `aup_student_academics_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aup_student_academics_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` varchar(200) NOT NULL,
  `course_id` int(11) NOT NULL,
  `course_status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `course_id` (`course_id`),
  KEY `student_id` (`student_id`,`course_id`),
  KEY `course_status` (`course_status`),
  CONSTRAINT `studentCourseHistoryToStudentID` FOREIGN KEY (`student_id`) REFERENCES `aup_student_academics` (`st_id`),
  CONSTRAINT `studentCourseID` FOREIGN KEY (`course_id`) REFERENCES `aup_courses` (`course_id`),
  CONSTRAINT `studentCourseStaus` FOREIGN KEY (`course_status`) REFERENCES `aup_student_course_status_list` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aup_student_academics_history`
--

LOCK TABLES `aup_student_academics_history` WRITE;
/*!40000 ALTER TABLE `aup_student_academics_history` DISABLE KEYS */;
INSERT INTO `aup_student_academics_history` VALUES (1,'a94542',2,3),(3,'a94542',3,2),(5,'a94542',5,3),(6,'a94542',6,1),(7,'a94542',7,3),(8,'a94542',8,1),(9,'a94542',9,3),(10,'a94542',10,3),(11,'a94542',12,3),(12,'a94542',13,3),(13,'a94542',15,1),(14,'a94542',17,2),(16,'a94542',20,3),(18,'a94542',4,3),(19,'a94542',33,2),(20,'a94542',34,2),(21,'a94542',35,3),(22,'a94542',38,3),(24,'a94542',39,3),(27,'a94542',16,1),(28,'a94542',18,3);
/*!40000 ALTER TABLE `aup_student_academics_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aup_student_course_status_list`
--

DROP TABLE IF EXISTS `aup_student_course_status_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aup_student_course_status_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(100) NOT NULL,
  `status_color` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aup_student_course_status_list`
--

LOCK TABLES `aup_student_course_status_list` WRITE;
/*!40000 ALTER TABLE `aup_student_course_status_list` DISABLE KEYS */;
INSERT INTO `aup_student_course_status_list` VALUES (1,'CURRENT',' #000'),(2,'FINISHED','#2B7F39'),(3,'NOT TAKEN','#D15656');
/*!40000 ALTER TABLE `aup_student_course_status_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aup_student_standing`
--

DROP TABLE IF EXISTS `aup_student_standing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aup_student_standing` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aup_student_standing`
--

LOCK TABLES `aup_student_standing` WRITE;
/*!40000 ALTER TABLE `aup_student_standing` DISABLE KEYS */;
INSERT INTO `aup_student_standing` VALUES (1,'Freshmen'),(2,'Sophomore'),(3,'Junior'),(4,'Senior');
/*!40000 ALTER TABLE `aup_student_standing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aup_students_login_info`
--

DROP TABLE IF EXISTS `aup_students_login_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aup_students_login_info` (
  `id` char(23) NOT NULL,
  `username` varchar(65) NOT NULL DEFAULT '',
  `password` varchar(65) NOT NULL DEFAULT '',
  `email` varchar(65) NOT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `mod_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `password_UNIQUE` (`password`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aup_students_login_info`
--

LOCK TABLES `aup_students_login_info` WRITE;
/*!40000 ALTER TABLE `aup_students_login_info` DISABLE KEYS */;
INSERT INTO `aup_students_login_info` VALUES ('141054567756c0bb7ea76c2','a94542','dc724af18fbdd4e59189f5fe768a5f8311527050','a94542@aup.edu',1,'2016-02-18 19:28:14'),('2877211756c610d6528c7','test','$2y$10$ZNP1h3lB8.ylp3sxzp347uvDkOmSRqdr2jPEVXwGiGV56brOGM.Kq','test@aup.edu',1,'2016-02-18 18:45:38');
/*!40000 ALTER TABLE `aup_students_login_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aup_undergrad_course_type`
--

DROP TABLE IF EXISTS `aup_undergrad_course_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aup_undergrad_course_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requirement_name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aup_undergrad_course_type`
--

LOCK TABLES `aup_undergrad_course_type` WRITE;
/*!40000 ALTER TABLE `aup_undergrad_course_type` DISABLE KEYS */;
INSERT INTO `aup_undergrad_course_type` VALUES (1,'General Education'),(2,'Core');
/*!40000 ALTER TABLE `aup_undergrad_course_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aup_undergrad_major_requirement`
--

DROP TABLE IF EXISTS `aup_undergrad_major_requirement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aup_undergrad_major_requirement` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `major_id` int(11) NOT NULL,
  `major_req_course_id` int(11) NOT NULL,
  `requirement_type` int(11) NOT NULL,
  PRIMARY KEY (`rid`),
  UNIQUE KEY `major_req_course_id` (`major_req_course_id`),
  KEY `major_id` (`major_id`,`major_req_course_id`),
  KEY `requirement_type` (`requirement_type`),
  KEY `requirementForMajorID` (`major_req_course_id`),
  CONSTRAINT `courseTypeGedOrCore` FOREIGN KEY (`requirement_type`) REFERENCES `aup_undergrad_course_type` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `majorRelation` FOREIGN KEY (`major_id`) REFERENCES `aup_undergrad_majors` (`mid`),
  CONSTRAINT `requirementForMajorID` FOREIGN KEY (`major_req_course_id`) REFERENCES `aup_courses` (`course_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aup_undergrad_major_requirement`
--

LOCK TABLES `aup_undergrad_major_requirement` WRITE;
/*!40000 ALTER TABLE `aup_undergrad_major_requirement` DISABLE KEYS */;
INSERT INTO `aup_undergrad_major_requirement` VALUES (1,1,2,2),(2,1,3,2),(3,1,4,2),(4,1,5,2),(5,1,6,2),(6,1,7,2),(7,1,8,2),(8,1,12,2),(9,1,13,2),(10,1,17,1),(12,1,15,1),(13,1,10,2),(14,1,9,2),(15,1,20,2),(16,1,33,1),(17,1,34,1),(22,1,35,2),(24,1,38,2),(26,1,39,1),(29,1,16,1),(30,1,18,2);
/*!40000 ALTER TABLE `aup_undergrad_major_requirement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aup_undergrad_majors`
--

DROP TABLE IF EXISTS `aup_undergrad_majors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aup_undergrad_majors` (
  `mid` int(11) NOT NULL AUTO_INCREMENT,
  `major_name` varchar(200) NOT NULL,
  `major_desc` text NOT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aup_undergrad_majors`
--

LOCK TABLES `aup_undergrad_majors` WRITE;
/*!40000 ALTER TABLE `aup_undergrad_majors` DISABLE KEYS */;
INSERT INTO `aup_undergrad_majors` VALUES (1,'Computer Science','Lorem Ipsum');
/*!40000 ALTER TABLE `aup_undergrad_majors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aup_undergrad_minor`
--

DROP TABLE IF EXISTS `aup_undergrad_minor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aup_undergrad_minor` (
  `mid` int(11) NOT NULL AUTO_INCREMENT,
  `minor_name` varchar(200) NOT NULL,
  `minor_desc` text NOT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aup_undergrad_minor`
--

LOCK TABLES `aup_undergrad_minor` WRITE;
/*!40000 ALTER TABLE `aup_undergrad_minor` DISABLE KEYS */;
INSERT INTO `aup_undergrad_minor` VALUES (1,'Minor in Applied Mathematics','Lorem Ipsum');
/*!40000 ALTER TABLE `aup_undergrad_minor` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-19  0:01:46
